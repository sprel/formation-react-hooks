import { useSelector as reduxSelector, shallowEqual, useDispatch } from 'react-redux'

const useSelector = selector => reduxSelector(selector, shallowEqual);

export { useDispatch, useSelector };