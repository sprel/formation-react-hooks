import React, { useCallback, memo } from 'react';
import './App.css';
import { useSelector, useDispatch } from './utils/redux';

import { selectView } from './actions/views';
import Playground from './Playground';

const VIEWS = [
  {label: "Why Hooks", id: "why-hooks"},
  {label: "Quels étaient les problèmes ?", id: "problem"},
  {label: "Avant de commencer", id: "careful"},
  {label: "Multiple setState", id: "multiple-render"},
  {label: "Memoization", id: "memoization"},
  {label: "Advanced Hooks", id: "advanced"},
  {label: "Redux et Router ?", id: "redux"},
  {label: "Custom Hooks", id: "custom"},
];

const App = () => {
  const view = useSelector(({views}) => views.view);
  const dispatch = useDispatch();

  const onSelectView = useCallback(toView => {
    dispatch(selectView(toView))
  }, [dispatch]);

  return (
    <div className="App">
      <div className="sidebar">
        {VIEWS.map(v => <Item
            key={v.id}
            id={v.id}
            label={v.label}
            onClick={onSelectView}
            active={view === v.id}
          />)}
      </div>
      <div className="playground">
          <Playground />
      </div>
    </div>
  );
}


const Item = memo(({id, label, onClick, active}) => {
  const onClickDiv = useCallback(() => onClick(id), [id, onClick]);
  return <button className={active ? 'btn btn--active' : "btn"} onClick={onClickDiv}>{label}</button>
});

export default App;