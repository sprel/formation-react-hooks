import { createAction} from 'redux-actions';

export const selectView = createAction("views::select");
export const updateInView = createAction('views::view::update');