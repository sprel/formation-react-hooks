import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import reducers from '../reducers';


const store = createStore(
	reducers,
	composeWithDevTools(
		applyMiddleware(thunkMiddleware),
	)
);

if (module.hot) {
	// Enable Webpack hot module replacement for reducers
	module.hot.accept('../reducers', () => {
		const nextRootReducer = require('../reducers/index');
		store.replaceReducer(nextRootReducer);
	});
}

export default store;

export const getStore = () => store.getState();
export const getDispatcher = () => store.dispatch;
