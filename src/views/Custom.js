import React, { memo, useCallback } from 'react';
import Code from '../components/Code';
import { useSelector, useDispatch } from '../utils/redux';

import { updateInView } from '../actions/views';

const lexique = {
    en: {
        title: "This is my app in EN",
        button: 'Change to'
    },
    fr: {
        title: 'This is my app in FR',
        button: 'Changer pour'
    },
};

const useI18n = () => {
    const locale = useSelector(({views:{custom}}) => custom.lang);

    return [lexique[locale], locale];
}

export default () => {
    return (
        <div>
            <h2>Custom Hooks</h2>
            <p>On a vu dans les règles qu'on ne pouvait créer des Hooks que dans des fonctions composant React. Il y a un autre endroit où on peut utiliser des Hooks : dans des Hooks !</p>
            <Code>
{`const lexique = {
    en: {
        title: "This is my app in EN",
        button: 'Change to'
    },
    fr: {
        title: 'This is my app in FR',
        button: 'Changer pour'
    },
};

const useI18n = () => {
    const locale = useSelector(({env}) => env.locale);

    return [lexique[locale], locale];
}


const MyFunc = () => {
    const [lexique, locale] = useI18n();

    const dispatch = useDispatch();

    const setLocale = useCallback(locale => dispatch(updateLocale(locale)), [dispatch])

    return <>
        <div>Current Locale is: {locale}</div>
        <div>Translation: {lexique.title}</div>
        <div>
            <LocaleSwitch id="fr" onClick={setLocale} />
            <LocaleSwitch id="en" onClick={setLocale} />
        </div>
    </>
}

const LocaleSwitch = memo(({id, onClick}) => {
    const [lexique] = useI18n();

    const onClickButton = useCallback(() => onClick(id), [onClick, id])

    return <button onClick={onClickButton}>{lexique.button} {id}</button>
})`}
            </Code>
            <MyFunc />
        </div>
    )
}

const MyFunc = () => {
    const [lexique, locale] = useI18n();

    const dispatch = useDispatch();

    const setLocale = useCallback(lang => dispatch(updateInView({view: 'custom', data: {lang}})), [dispatch])

    return <>
        <div>Current Locale is: {locale}</div>
        <div>Translation: {lexique.title}</div>
        <div>
            <LocaleSwitch id="fr" onClick={setLocale} />
            <LocaleSwitch id="en" onClick={setLocale} />
        </div>
    </>
}

const LocaleSwitch = memo(({id, onClick}) => {
    const [lexique] = useI18n();

    const onClickButton = useCallback(() => onClick(id), [onClick, id])

    return <button onClick={onClickButton}>{lexique.button} {id}</button>
})