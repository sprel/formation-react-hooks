import React, { useCallback } from 'react';
import Code from '../components/Code';
import { useSelector, useDispatch } from '../utils/redux';
import { updateInView } from '../actions/views';

export default () => {
    return (
        <div>
            <h4>Et Redux dans tout ca ?</h4>
            <p>Redux propose des Hooks depuis la version 7.1</p>
            <Code>
{`import { updateValue } from '../actions/myForm';

const MyFunc = () => {
    const { firstName, lastName } = useSelector(({myForm}) => myForm);
    const dispatch = useDispatch();

    const onChangeFirstName = useCallback(({target:{value}}) => dispatch(updateValue({firstName: value})),[dispatch]);
    const onChangeLastName = useCallback(({target:{value}}) => dispatch(updateValue({lastName: value})),[dispatch]);

    return <>
        <input type="text" value={firstName} onChange={onChangeFirstName} placeholder="first name" />
        <input type="text" value={lastName} onChange={onChangeLastName} placeholder="last name" />
    </>
}`}
            </Code>

            <MyFunc />
            <h4>Comment le composant sait que des valeurs ont changé ?</h4>
            <p><b>useSelector</b> va comparer son retour précédent avec le nouveau, si une des valeurs est différente, le composant sera re-render. Le problème est que, par defaut, useSelector fait une comparaison stricte, ce qui est trop léger. Il faut donc légèrement "pimper" le useSelector par défaut</p>
            <Code>
{`import { useSelector, shallowEqual } from 'react-redux'

export default selector => useSelector(selector, shallowEqual);
`}
            </Code>
            <p>On pourra ensuite utiliser notre propre selector, qui fera bien un <b>shallowCompare</b> entre ses différentes invocations</p>
            <h4>Eviter les render inutiles</h4>
            <p>On pourrait etre tenté de faire comme avec le <b>connect</b> actuel</p>
            <Code>
{`const { maVar1, maVar2, maVar3} = useSelector(({views}) => views.profile)`}
            </Code>
            <p>Le problème de ce code, est qu'il va retourner toute la vue "profile", la comparaison se fera donc sur toutes les clés de 'views.profile' et non pas uniquement sur les 3 récupérées dans le code. Le bonne façon de faire est donc</p>
            <Code>
{`const { maVar1, maVar2, maVar3 } = useSelector(({views}) => ({
    maVar1: views.profile.maVar1,
    mavar2: views.profile.maVar2,
    maVar3: views.profile.maVar3,
}))`}
            </Code>
            <p>A noter également que vous pouvez utiliser plusieurs useSelector. On code dans ce genre est également possible</p>
            <Code>
{`const maVar1 = useSelector(({views}) => views.profile.maVar1)
const maVar2 = useSelector(({views}) => views.profile.maVar2)
const maVar3 = useSelector(({views}) => views.profile.maVar3)`}
            </Code>

            <h4>Comment ca se passe le contrôle des données du coup ?</h4>
            <p>Le controle des données va devoir s'effectuer au sein d'une action pour éviter la re-définition de la fonction de contrôle</p>
            <Code>
{`const MyFunc = () => {
    
    // ...

    const onSubmitForm = useCallback(() => {
        if (firstName.trim() === "") return false;
        if (lastName.trim() === "") return false;

        // Good, call Api !
    }, [firstName, lastName]);

    return <>
        <button onClick={onSubmitForm}>Valider</button>
    </>
}`}
            </Code>
           <p>A chaque changement de firstName, ou lastName, le fonction onSubmitForm va etre redéfini, nous avons donc des modification de composant non voulu. Le code valide serait :</p>
           <Code>
{`const MyFunc = () => {
    
    // ...

    const onSubmitForm = useCallback(() => dispatch(sendForm()), [dispatch]);

    return <>
        <button onClick={onSubmitForm}>Valider</button>
    </>
}

// in actions
const sendForm = () => (dispatch, getState) => {
    const { firstName, lastName } = getState().myForm;

    // Do some logic here....
}

`}
            </Code>
            <h4>Classic error case</h4>
            <p>Le cas d'erreur le plus classique : on récupère une variable depuis Redux, et on fait un immediate render</p>
            <Code>
{`const MyFunc = () => {
    
    const { isLoading, hasDatas } = useSelector(({myForm}) => ({
        isLoading: myForm.loading,
        hasDatas: myForm.datas && myForm.datas.length > 0
    });

    if (!isLoading) return <div>Loading</div>;
    if (!hasDatas) return <div>Sorry No Datas</div>;

    const [count, setCount] = useState(0),


    // ...
}`}
            </Code>
            <p>Un premier hook est défini avec le <b>useSelector</b> Lorsque hasDatas ou isLoading change, le nombre de Hooks change également, ce qui est contraires aux règles > ca plante</p>
            <h4>Et ReactRouter ?</h4>
            <p>Not yet, c'est planned dans la prochaine version majeure</p>
        </div>
    )
}

const MyFunc = () => {
    const { firstName, lastName } = useSelector(({views:{redux}}) => redux);
    const dispatch = useDispatch();

    const onChangeFirstName = useCallback(({target:{value}}) => dispatch(updateInView({view: 'redux', data: {firstName: value}})),[dispatch]);
    const onChangeLastName = useCallback(({target:{value}}) => dispatch(updateInView({view: 'redux', data: {lastName: value}})),[dispatch]);

    return <>
        <input type="text" value={firstName} onChange={onChangeFirstName} placeholder="first name" />
        <input type="text" value={lastName} onChange={onChangeLastName} placeholder="last name" />
    </>
}