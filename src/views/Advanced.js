import React, { useState, useCallback, useEffect, memo } from 'react';
import Code from '../components/Code';

export default () => {
    return (
        <div>
            <h2>useEffect</h2>
            <p>useEffect permet de remplacer <b>componentDidMount</b>, <b>componentDidUpdate</b> et <b>componentWillUnmount</b> au sein de la meme fonction</p>
            <Code>
{`useEffect(function, dependencies)`}
            </Code>
            <p>useEffect avec un tableau de dependencies vide ne sera appelé qu'une seule fois, il correspond au <b>componentDidMount</b></p>
            <Code>
{`const MyFunc = () => {

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("Example 1 : useEffect")
    }, []);

    const onClick = useCallback(() => setCount(current => current + 1), []);

    return <button onClick={onClick}>IncMe {count}</button>
}`}
            </Code>
            <MyFunc />
            <p>useEffect peut retourner une fonction, qui sera executée avant chaque nouvel appel à useEffect (et avant chaque modification d'une des valeur dans dependencies), une fonction useEffect avec un tabealu de dependincies vide, qui retourne une fonction, correspond à un <b>componentWillUnmount</b></p>
            <Code>
{`const MyFunc = () => {

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("Example 2 : useEffect")
        return () => {
            console.log('Example 2 : componentWillUnmount');
        }
    }, []);

    const onClick = useCallback(() => setCount(current => current + 1), []);

    return <button onClick={onClick}>IncMe {count}</button>
}`}
            </Code>
            <MyFunc2 />
            <p>Combinaison avec une dependency</p>
            <Code>
{`const MyFunc = () => {

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("Example 3 : count is now", count)
        return () => {
            console.log('Example 3 : Count was ', count);
        }
    }, [count]);

    const onClick = useCallback(() => setCount(current => current + 1), []);

    return <button onClick={onClick}>IncMe {count}</button>
}`}
            </Code>
            <MyFunc3 />
            <p>Concrètement à quoi ca sert ? La fonction retournée va principalement servir à détacher des events ajoutés dans le useEffect initial, détruire des interval, timers,...</p>
            <p><b>Important</b> : Il est possible d'avoir plusieurs fonctions useEffect, qui seront executées dans l'ordre d'apparition. Il est recommandé de séparer les <b>componentDidMount</b> / <b>componentWillUnmount</b> dans une fonction propre. De même lorsque la complexité dun <b>componentDidUpdate</b> devient trop grande (avec des if dans tous les sens), préférez utiliser plusieurs useEffect</p>
            <Code>
{`const MyFunc = () => {

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("Example 4 has Mounted ")
        return () => {
            console.log('Example 4 will unmount ');
        }
    }, []);

    useEffect(() => {
        console.log("Example 4 : count is now", count)
    }, [count]);

    const onClick = useCallback(() => setCount(current => current + 1), []);

    return <button onClick={onClick}>IncMe {count}</button>
}`}
            </Code>
            <MyFunc4 />
            <h4>Fetch data</h4>
            <p>useEffect ne peux pas prendre en paramètre une fonction asynchrone. Le code suivant est donc invalide</p>
            <Code>
{`const MyFunc = () => {
    useEffect(async () => {
        await doSomething()
    }, []);
}`}
            </Code>
            <p>Il suffit pour corriger ca de définir une fonction au sein de useEffect</p>
            <Code>
{`const MyFunc = () => {
    useEffect(() => {
        const getDatas = async () => {
            await doSomething()
        }
        getDatas();
    }, []);
}`}
            </Code>
            <h2>Chained functions</h2>
            <p>Dans les formulaires, ou les maps, on aime bien pouvoir faire du chainage de fonction du type</p>
            <Code>
{`onChange = key => value => this.setState({[key]: value});`}
            </Code>
            <h4>Comment faire ca avec des Hooks ?</h4>
            <p>Il faut déjà comprendre, que meme dans l'état actuel, ce type de fonction, bien que pratique, n'est pas optimal car non mémoizé, même dane le cadre d'une classe. Il faudrait memoizer la fonction chainée pour eviter les render inutiles</p>
            <Code>
{`onChange = key => memoize(value => this.setState({[key]: value}));`}
            </Code>
            <p>La première piste serait de donc chainer les useCallback</p>
            <Code>
{`const MyFunc = () => {
    const [clicked, setClicked] = useState(0);

    const onClickButton = useCallback(i => {
        return useCallback(() => {
            setClicked(i)  
        }, [i])
    }, []);

    return <>
        {Array.from({length: 5}).map((_, i) => <button key={i} onClick={onClickButton(i)}>Click me {i}</button>)}
        current clicked is {clicked}
    </>
}`}
            </Code>
            <MyFunc5 />
            <p>2 problèmes dans ce cas : Tout d'abord on enfreint la règle des hooks qui dit <b>Jamais à l’intérieur de boucles, de code conditionnel ou de fonctions imbriquées.</b><br />Et ensuite, prenons cet exemple :</p>
            <Code>
{`const MyFunc = () => {
    const [clicked, setClicked] = useState(0);
    const [nbButtons, setNbButtons] = useState(5);

    const onClickButton = useCallback(i => useCallback(() => setClicked(i), [i]), []);

    const onAddButton = useCallback(() => setNbButtons(current => current+1), [])

    return <>
        {Array.from({length: nbButtons}).map((_, i) => <button key={i} onClick={onClickButton(i)}>Click me {i}</button>)}
        current clicked is {clicked}
        <button onClick={onAddButton}>Add a button</button>
    </>
}`}
            </Code>
            <MyFunc6 />
            <p>Le tableau n'étant pas fixe, on peut définir un nombre différent de Hooks suivant les renders.</p>
            <h4>Mauvaise idée donc, comment faire alors ?</h4>
            <p>Le but des hooks est de simplifier l'écriture des composants. On pourra donc définir plus facilement et plus rapidement des composant, tout en les découpant encore plus</p>
            <Code>
{`const MyFunc7 = () => {
    const [clicked, setClicked] = useState(0);
    const [nbButtons, setNbButtons] = useState(5);

    const onClickButton = useCallback(i => setClicked(i), []);
    const onAddButton = useCallback(() => setNbButtons(current => current+1), [])

    return <>
        {Array.from({length: nbButtons}).map((_, i) => <Button key={i} id={i} onClick={onClickButton} />)}
        current clicked is {clicked}
        <button onClick={onAddButton}>Add a button</button>
    </>
}

const Button = memo(({onClick, id}) => {
    const onClickButton = useCallback(() => onClick(id), [onClick, id]);
    return <button onClick={onClickButton}>Click me {id}</button>
});`}
            </Code>
            <MyFunc7 />
            <h2>Controle des données</h2>
            <h4>J'ai un gros formulaire, et je dois faire des controles de données avant de call l'api, je fais ca comment ?</h4>
            <p>On a déjà vu que les données sont figées dans un useCallback, on ne peut donc pas utiliser un code du genre :</p>
            <Code>
{`const myFunc = () => {
    const [state, setState] = useState({
        firstName: '',
        lastName: '',
        email: ''
    })

    const onSubmit = useCallback(() => {
        if (state.firstName.trim() === '') return false;

        // Alright, call the API
    }, [])
}`}
            </Code>
            <p>Les données étant figées, firstName serait toujours une chaine vide</p>
            <p>Il n'y a également aucun intérêt à faire quelque chose du genre</p>
            <Code>
{`const myFunc = () => {
    const [state, setState] = useState({
        firstName: '',
        lastName: '',
        email: ''
    })

    const onSubmit = useCallback(() => {
        if (state.firstName.trim() === '') return false;

        // Alright, call the API
    }, [state])
}`}
            </Code>
            <p>Le callback serait redefini à chaque changement d'une valeur du state</p>
            <p>On pourrait essayer de "limiter la casse" et n'inclure que les valeurs necessitant un controle</p>
            <Code>
{`const myFunc = () => {
    const [state, setState] = useState({
        firstName: '',
        lastName: '',
        email: ''
    })

    const onSubmit = useCallback(() => {
        if (state.firstName.trim() === '') return false;

        // Alright, call the API
    }, [state.firstName])
}`}
            </Code>
            <p>Mais on créérait quand meme un bon nombre de rendus inutiles.</p>
            <h4>Alors, la solution ?!</h4>
            <p>On va se servir de la function du setState, ainsi que du useEffect</p>
            <Code>
{`const MyFunc = () => {
    const [state, setState] = useState({
        firstName: '',
        lastName: '',
        errors: [],
        formValid: false,
    });

    useEffect(() => {
        if (state.formValid) {
            console.log("FORM IS VALID ! CALL API !")
            // Do API Call
        }
    }, [state.formValid])

    const onChangeFirstName = useCallback(({target:{value}}) => setState(current => ({...current, firstName: value})), [])
    const onChangeLastName = useCallback(({target:{value}}) => setState(current => ({...current, lastName: value})), [])

    const onSubmitForm = useCallback(() => setState(current => {
        if (current.firstName.trim() === '') return {...current, errors: ['firstName']}

        return {
            ...current,
            errors: [],
            formValid: true,
        }
    }), [])

    return (<>
        <input type="text" value={state.firstName} onChange={onChangeFirstName} placeholder="first name" />
        <input type="text" value={state.lastName} onChange={onChangeLastName} placeholder="last name" />
        <button onClick={onSubmitForm}>Valider</button>
        {state.errors.includes('firstName') && <p style={{color: 'red'}}>Error, firstName cannot be empty</p>}
        {state.formValid && <p style={{color: 'green'}}>Form is valid!</p>}
    </>)
}`}
            </Code>
            <MyFunc8 />
            <h2>et memo dans tous ca ?</h2>
            <h4>Dois-je toujours utiliser memo pour mes functions "propless"</h4>
            <Code>{`
const MyFunc = () => {
    const [count, setCount] = useState(0);
    const inCount = useCallback(() => setCount(count => count + 1), [])
    return (<>
        <div>
            Count is {count}
            <button onClick={inCount}>Inc Me</button>
        </div>
        <InnerCounter />
        <InnerMemoCounter />
    </>)
}

const InnerCounter = () => {
    const [count, setCount] = useState(0);
    const inCount = useCallback(() => setCount(count => count + 1), [])
    console.log("Render InnerCounter")
    return (<>
        <div>
            InnerCount is {count}
            <button onClick={inCount}>Inc Me</button>
        </div>
    </>)
}

const InnerMemoCounter = memo(() => {
    const [count, setCount] = useState(0);
    const inCount = useCallback(() => setCount(count => count + 1), []);
    console.log("Render InnerMemoCounter")
    return (<>
        <div>
            InnerMemoCount is {count}
            <button onClick={inCount}>Inc Me</button>
        </div>
    </>)
});
`}
            </Code>
            <MyFunc9 />
        </div>
    )
}

const MyFunc = () => {

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("Example 1 : useEffect")
    }, []);

    const onClick = useCallback(() => setCount(current => current + 1), []);

    return <button onClick={onClick}>IncMe {count}</button>
}

const MyFunc2 = () => {

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("Example 2 : useEffect")
        return () => {
            console.log('Example 2 : componentWillUnmount');
        }
    }, []);

    const onClick = useCallback(() => setCount(current => current + 1), []);

    return <button onClick={onClick}>IncMe {count}</button>
}


const MyFunc3 = () => {

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("Example 3 : count is now", count)
        return () => {
            console.log('Example 3 : Count was ', count);
        }
    }, [count]);

    const onClick = useCallback(() => setCount(current => current + 1), []);

    return <button onClick={onClick}>IncMe {count}</button>
}

const MyFunc4 = () => {

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("Example 4 has Mounted ")
        return () => {
            console.log('Example 4 will unmount ');
        }
    }, []);

    useEffect(() => {
        console.log("Example 4 : count is now", count)
    }, [count]);

    const onClick = useCallback(() => setCount(current => current + 1), []);

    return <button onClick={onClick}>IncMe {count}</button>
}


const MyFunc5 = () => {
    const [clicked, setClicked] = useState(0);

    const onClickButton = useCallback(i => {
        return useCallback(() => { // eslint-disable-line
            setClicked(i)  
        }, [i])
    }, []);

    return <>
        {Array.from({length: 5}).map((_, i) => <button key={i} onClick={onClickButton(i)}>Click me {i}</button>)}
        current clicked is {clicked}
    </>
}

const MyFunc6 = () => {
    const [clicked, setClicked] = useState(0);
    const [nbButtons, setNbButtons] = useState(5);

    const onClickButton = useCallback(i => useCallback(() => setClicked(i), [i]), []); //eslint-disable-line

    const onAddButton = useCallback(() => setNbButtons(current => current+1), [])

    return <>
        {Array.from({length: nbButtons}).map((_, i) => <button key={i} onClick={onClickButton(i)}>Click me {i}</button>)}
        current clicked is {clicked}
        <button onClick={onAddButton}>Add a button</button>
    </>
}

const MyFunc7 = () => {
    const [clicked, setClicked] = useState(0);
    const [nbButtons, setNbButtons] = useState(5);

    const onClickButton = useCallback(i => setClicked(i), []);
    const onAddButton = useCallback(() => setNbButtons(current => current+1), [])

    return <>
        {Array.from({length: nbButtons}).map((_, i) => <Button key={i} id={i} onClick={onClickButton} />)}
        current clicked is {clicked}
        <button onClick={onAddButton}>Add a button</button>
    </>
}

const Button = memo(({onClick, id}) => {
    const onClickButton = useCallback(() => onClick(id), [onClick, id]);
    return <button onClick={onClickButton}>Click me {id}</button>
});


const MyFunc8 = () => {
    const [state, setState] = useState({
        firstName: '',
        lastName: '',
        errors: [],
        formValid: false,
    });

    useEffect(() => {
        if (state.formValid) {
            console.log("FORM IS VALID ! CALL API !")
            // Do API Call
        }
    }, [state.formValid])

    const onChangeFirstName = useCallback(({target:{value}}) => setState(current => ({...current, firstName: value})), [])
    const onChangeLastName = useCallback(({target:{value}}) => setState(current => ({...current, lastName: value})), [])

    const onSubmitForm = useCallback(() => setState(current => {
        if (current.firstName.trim() === '') return {...current, errors: ['firstName']}

        return {
            ...current,
            errors: [],
            formValid: true,
        }
    }), [])

    return (<>
        <input type="text" value={state.firstName} onChange={onChangeFirstName} placeholder="first name" />
        <input type="text" value={state.lastName} onChange={onChangeLastName} placeholder="last name" />
        <button onClick={onSubmitForm}>Valider</button>
        {state.errors.includes('firstName') && <p style={{color: 'red'}}>Error, firstName cannot be empty</p>}
        {state.formValid && <p style={{color: 'green'}}>Form is valid!</p>}
    </>)
}

const MyFunc9 = () => {
    const [count, setCount] = useState(0);
    const inCount = useCallback(() => setCount(count => count + 1), [])
    return (<>
        <div>
            Count is {count}
            <button onClick={inCount}>Inc Me</button>
        </div>
        <InnerCounter />
        <InnerMemoCounter />
    </>)
}

const InnerCounter = () => {
    const [count, setCount] = useState(0);
    const inCount = useCallback(() => setCount(count => count + 1), [])
    console.log("Render InnerCounter")
    return (<>
        <div>
            InnerCount is {count}
            <button onClick={inCount}>Inc Me</button>
        </div>
    </>)
}

const InnerMemoCounter = memo(() => {
    const [count, setCount] = useState(0);
    const inCount = useCallback(() => setCount(count => count + 1), []);
    console.log("Render InnerMemoCounter")
    return (<>
        <div>
            InnerMemoCount is {count}
            <button onClick={inCount}>Inc Me</button>
        </div>
    </>)
});