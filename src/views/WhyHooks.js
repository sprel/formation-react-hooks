import React from 'react';

export default () => {
    return (
        <div>
            <h4>C'est quoi un Hook ?</h4>
            Les Hooks sont des fonctions qui permettent de « se brancher » sur la gestion d’état local et de cycle de vie de React depuis des fonctions composants. Les Hooks ne fonctionnent pas dans des classes : ils permettent d’utiliser React sans classes.
            <h4>Pourquoi les Hooks ?</h4>
            <ul>
                <li>Parce que c'est l'évolution naturelle de React</li>
                <li>Plus besoin d'avoir à gérer des classes</li>
                <li>Après le <strong>stateless</strong>, welcome to <strong>propless</strong></li>
                <li>Allez lire le long article de React qui explique en détail : <a href="https://fr.reactjs.org/docs/hooks-intro.html#motivation">https://fr.reactjs.org/docs/hooks-intro.html#motivation</a></li>
            </ul>
            <h4>Les règles des Hooks</h4>
            <ul>
                <li>Commencent par <b>use</b></li>
                <li>Uniquement depuis des <b>fonctions composants React</b>. Jamais depuis des fonctions JavaScript classiques</li>
                <li>Uniquement au <b>niveau racine</b>. Jamais à l’intérieur de boucles, de code conditionnel ou de fonctions imbriquées.</li>
            </ul>
        </div>
    )
}
