import React, { useState, useCallback, memo, useMemo } from 'react';
import Code from '../components/Code';

export default () => {
    return (
        <div>
            <h4>useCallback</h4>
            <p>UseCallback retourne une version memoizée de la fonction.</p>
            <Code>
{`useCallback(function, dependencies)`}
            </Code>
            <p>useCallback prend en premier paramètre une fonction, où toutes les variables utilisées seront figée à <b>l'instant t</b><br />Ainsi qu'un tableau de dependencies qui aura pour effet de relancer la memoization si une de ses valeures change.<br /><b>Attention :</b> useCallback sans 2eme paramètre n'a aucun effet, et ne memoize rien</p>
            
            <Code>
{`const MyFunc = () => {
    const [count, setCount] = useState(0);

    const onClickInc = useCallback(() => setCount(count+1), [])
    const onClickDec = useCallback(() => setCount(count-1), [])

    return <>
        <Button onClick={onClickInc} label="Inc me!" />
        <Button onClick={onClickDec} label="Dec me!" />
        Count value is: {count}
    </>
}`}
            </Code>
            <MyFunc />
            <h4>Comment faire donc ?</h4>
           <p>Passer count en paramètre de useCallback ?</p>
           <Code>
{`const MyFunc = () => {
    const [count, setCount] = useState(0);

    const onClickInc = useCallback(() => setCount(count+1), [count])
    const onClickDec = useCallback(() => setCount(count-1), [count])

    return <>
        <Button onClick={onClickInc} label="Inc me!" />
        <Button onClick={onClickDec} label="Dec me!" />
        Count value is: {count}
    </>
}`}
            </Code>
            <MyFunc2 />
            <h4>What is the solution?</h4>
            <p>Les fonction setState (ici setCount) peuvent prendre en paramètre une fonction, qui aura pour argument l'état actuel du state. Le retour de cette fonction définira le nouveau state.<br /><b>Rappel :</b> setState ne fait pas de merge automatique. Dans le cas d'un state Array ou Object, il faudra dupliquer toutes les valeurs du current state pour ne modifier que les clés voulues</p>
            <Code>
{`const MyFunc = () => {
    const [count, setCount] = useState(0);

    const onClickInc = useCallback(() => setCount(current => current+1), [])
    const onClickDec = useCallback(() => setCount(current => current-1), [])

    return <>
        <Button onClick={onClickInc} label="Inc me!" />
        <Button onClick={onClickDec} label="Dec me!" />
        Count value is: {count}
    </>
}`}
            </Code>
            <MyFunc3 />
            <h4>Quelle est la différence avec useMemo ?</h4>
            <p>useMemo va immediatement lancer la fonction est mémoriser son résultat. On va plutot utiliser useMemo pour des partials renders, de gros calculs, ou des définitions de dataset</p>
            <Code>
{`const MyFunc = () => {
    const myfunc = () => 5;

    const withCallback = useCallback(myfunc, []);
    const withMemo = useMemo(myfunc, [])

    // withCallback() === withMemo
}`}
            </Code>
            <p>Notez la différence entre useCallback qui retourne une fonction, quand useMemo retourne le résultat de la fonction</p>
            <p>On a donc bien le retour de l'appel de <b>withCallback()</b> qui est égal à la valeur de <b>withMemo</b></p>
            <MyFunc4 />
        </div>
    )
}

const MyFunc = () => {
    const [count, setCount] = useState(0);

    const onClickInc = useCallback(() => setCount(count+1), []) //eslint-disable-line
    const onClickDec = useCallback(() => setCount(count-1), []) //eslint-disable-line

    return <>
        <Button onClick={onClickInc} label="Inc me!" />
        <Button onClick={onClickDec} label="Dec me!" />
        Count value is: {count}
    </>
}

const Button = memo(({onClick, label}) => {

    console.log("Render!", label);

    return <button onClick={onClick}>{label}</button>

})

const MyFunc2 = () => {
    const [count, setCount] = useState(0);

    const onClickInc = useCallback(() => setCount(count+1), [count])
    const onClickDec = useCallback(() => setCount(count-1), [count])

    return <>
        <Button onClick={onClickInc} label="Inc me!" />
        <Button onClick={onClickDec} label="Dec me!" />
        Count value is: {count}
    </>
}

const MyFunc3 = () => {
    const [count, setCount] = useState(0);

    const onClickInc = useCallback(() => setCount(current => current+1), [])
    const onClickDec = useCallback(() => setCount(current => current-1), [])

    return <>
        <Button onClick={onClickInc} label="Inc me!" />
        <Button onClick={onClickDec} label="Dec me!" />
        Count value is: {count}
    </>
}

const MyFunc4 = () => {
    const myfunc = () => 5;

    const withCallback = useCallback(myfunc, []);
    const withMemo = useMemo(myfunc, [])

    console.log("withCallback()", withCallback(), "withMemo", withMemo)

    return null;
}