import React, { useState, memo } from 'react';
import Code from '../components/Code';

export default () => {
    return (
        <div>
            <h4>A savoir avant d'utiliser les hooks</h4>
            <p>Les Hooks sont des utilisés comme des objet système par React pour valider l'intégrité du composant fonctionnel. <br />Ce qui implique que le nombre de Hooks déclarés doit être <strong>identique entre chaque render</strong></p>
            <Code>
{`const MyFunc = () => {
    const [isLoading, setIsLoading] = useState(true);

    return <>
        <Loading loading={isLoading} />
        {isLoading && <button onClick={() => setIsLoading(false)}>Finish load</button>}
    </>
}

const Loading = memo(({loading}) => {
    const [countLoading, setCountLoading] = useState(0);
    if (loading) return <div>
        Ca charge Michel !
        <button onClick={() => setCountLoading(countLoading + 1)}>Inc Me! {countLoading}</button>
    </div>

    const [count, setCount] = useState(0);

    return <button onClick={() => setCount(count + 1)}>IncMe! {count}</button>
})`}
            </Code>

            <MyFunc />

            <h4>Cas particulier</h4>
            <p>Le seul cas accepté par React, lorsqu'un composant passe de <b>n</b> Hooks à <b>aucun</b>, et inversement</p>
            <Code>
{`const MyFunc = () => {
    const [isLoading, setIsLoading] = useState(true);

    return <>
        <Loading loading={isLoading} />
        <button onClick={() => setIsLoading(!isLoading)}>{isLoading ? 'Finish' : 'Start'} load</button>
    </>
}

const Loading = memo(({loading}) => {
    if (loading) return <div>Ca charge Michel !</div>

    const [count, setCount] = useState(0);
    return <button onClick={() => setCount(count + 1)}>IncMe! {count}</button>
})`}
            </Code>

            <MyFunc2 />

        </div>
    )
}

const MyFunc = () => {
    const [isLoading, setIsLoading] = useState(true);

    return <>
        <Loading loading={isLoading} />
        {isLoading && <button onClick={() => setIsLoading(false)}>Finish load</button>}
    </>
}

const Loading = memo(({loading}) => {
    const [countLoading, setCountLoading] = useState(0);
    if (loading) return <div>
        Ca charge Michel !
        <button onClick={() => setCountLoading(countLoading + 1)}>Inc Me! {countLoading}</button>
    </div>

    const [count, setCount] = useState(0);

    return <button onClick={() => setCount(count + 1)}>IncMe! {count}</button>
})

const MyFunc2 = () => {
    const [isLoading, setIsLoading] = useState(true);

    return <>
        <Loading2 loading={isLoading} />
        <button onClick={() => setIsLoading(!isLoading)}>{isLoading ? 'Finish' : 'Start'} load</button>
    </>
}

const Loading2 = memo(({loading}) => {
    if (loading) return <div>Ca charge Michel !</div>

    const [count, setCount] = useState(0);
    return <button onClick={() => setCount(count + 1)}>IncMe! {count}</button>
})