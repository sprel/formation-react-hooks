import React, { useState, memo } from 'react';
import Code from '../components/Code';

export default () => {
    return (
        <div>
            <h2>C'etait quoi déjà les problèmes avec les Hooks ?</h2>
            <h4>La memoization</h4>
            <Code>
{`const MyFunc = () => {
    const [count, setCount] = useState(0);
    return <>
        <Button onClick={() => setCount(count+1)} label="Inc Me!" />
        <Button onClick={() => setCount(count-1)} label="Dec Me!" />
        Counter value: {count}
    </>
}

const Button = memo(({onClick, label}) => {
    console.log("Render Button", label)

    return <button onClick={onClick}>{label}</button>
})

`}
            </Code>

            <MyFunc />

            <h4>Les multiples setState / renders</h4>
            <Code>
{`const MyFunc2 = () => {
    const [value, setValue] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const onClickSubmit = () => {
        setIsLoading(true);
        setValue('Ca Charge!');
    }

    console.log("render!");
    return <>
        <input type="text" onChange={({target:{value}}) => setValue(value)} value={value} />
        <button onClick={onClickSubmit} disabled={isLoading}>Submit</button>
    </>
}`}  
            </Code>
            <MyFunc2 />
        </div>
    )
}

const MyFunc = () => {
    const [count, setCount] = useState(0);
    return <>
        <Button onClick={() => setCount(count+1)} label="Inc Me!" />
        <Button onClick={() => setCount(count-1)} label="Dec Me!" />
        Counter value: {count}
    </>
}

const Button = memo(({onClick, label}) => {
    console.log("Render Button", label)

    return <button onClick={onClick}>{label}</button>
})

const MyFunc2 = () => {
    const [value, setValue] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const onClickSubmit = () => {
        setIsLoading(true);
        setValue('Ca Charge!');
    }

    console.log("render!");
    return <>
        <input disabled={isLoading} type="text" onChange={({target:{value}}) => setValue(value)} value={value} />
        <button onClick={onClickSubmit} disabled={isLoading}>Submit</button>
    </>
}