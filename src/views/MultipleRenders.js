import React, { useState } from 'react';
import Code from '../components/Code';

export default () => {
    return (
        <div>
            <h2>Le problème des multiple setState</h2>
            <h4>Merci React</h4>
            <p>A partir de <strong>React 17</strong> aussi bien les setState que les Hooks.useState seront batchés nativement par React<br />Il semblerait que ce comportement soit activé en undocumented depuis React 16.9</p>
            

            <h4>Le state global</h4>
            <Code>
{`const MyFunc = () => {
    const [state, setState] = useState({
        firstName: '',
        lastName: '',
        email: '',
        loading: false,
    });

    const changeFirstName = ({target:{value}}) => setState({...state, firstName: value});
    const changeLastName = ({target:{value}}) => setState({...state, lastName: value});
    const changeEmail = ({target:{value}}) => setState({...state, email: value});
    const onSubmit = () => setState({
        loading: true,
        firstName: 'Ca charge',
        lastName: 'Ca charge',
        email: 'Ca charge',
    });

    return <>
        <input type="text" value={state.firstName} placeholder='Prénom' onChange={changeFirstName} disabled={state.loading} />
        <input type="text" value={state.lastName} placeholder='Nom' onChange={changeLastName} disabled={state.loading} />
        <input type="text" value={state.email} placeholder='Email' onChange={changeEmail} disabled={state.loading} />
        <button onClick={onSubmit} disabled={state.loading}>Valider</button>
    </>
}`}
            </Code>
            <p><b>Attention : </b>Contairement à <b>setState</b>, les Hooks ne font pas un merge. Il faut donc impérativement, à chaque setState, dupliquer l'ensemble du state dans le nouveau state</p>

            <MyFunc />

        </div>
    )
}

const MyFunc = () => {
    const [state, setState] = useState({
        firstName: '',
        lastName: '',
        email: '',
        loading: false,
    });

    const changeFirstName = ({target:{value}}) => setState({...state, firstName: value});
    const changeLastName = ({target:{value}}) => setState({...state, lastName: value});
    const changeEmail = ({target:{value}}) => setState({...state, email: value});
    const onSubmit = () => setState({
        loading: true,
        firstName: 'Ca charge',
        lastName: 'Ca charge',
        email: 'Ca charge',
    });

    return <>
        <input type="text" value={state.firstName} placeholder='Prénom' onChange={changeFirstName} disabled={state.loading} />
        <input type="text" value={state.lastName} placeholder='Nom' onChange={changeLastName} disabled={state.loading} />
        <input type="text" value={state.email} placeholder='Email' onChange={changeEmail} disabled={state.loading} />
        <button onClick={onSubmit} disabled={state.loading}>Valider</button>
    </>
}
