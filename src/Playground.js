import React from 'react';
import { useSelector } from './utils/redux';
import WhyHooks from './views/WhyHooks';
import Problem from './views/Problem';
import Careful from './views/Careful';
import MultipleRenders from './views/MultipleRenders';
import Redux from './views/Redux';
import Memoization from './views/Memoization';
import Advanced from './views/Advanced';
import Custom from './views/Custom';

export default () => {
    const view = useSelector(({views}) => views.view);
    
    switch (view) {
        default:
        case "why-hooks":
            return <WhyHooks />
        case "problem":
            return <Problem />
        case 'careful':
            return <Careful />
        case 'multiple-render':
            return <MultipleRenders />
        case 'memoization':
            return <Memoization />
        case 'advanced':
            return <Advanced />
        case 'redux':
            return <Redux />
        case "custom":
            return <Custom />
    }
}