import { createReducer } from 'redux-create-reducer';
import update from 'immutability-helper';

const initialState = {
	view: 'why-hooks',
	redux: {
		firstName: '',
		lastName: '',
	},
	custom: {
		lang: 'fr',
	}
};

const reducers = {
	"views::select": (state, {payload:view}) => update(state, {
        view: { $set: view },
	} ),
	"views::view::update": (state, {payload:{view, data}}) => update(state, {
        [view]: { $merge: data },
	} ),
}


export default createReducer(initialState, reducers);
