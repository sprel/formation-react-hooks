import React, { memo } from 'react';
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter';
import theme from 'react-syntax-highlighter/dist/esm/styles/prism/atom-dark';

export default memo(({children}) => <SyntaxHighlighter language="jsx" style={theme} showLineNumbers>
    {children}
</SyntaxHighlighter>
);